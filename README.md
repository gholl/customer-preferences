# Customer Preferences Centre

## Requirements

You should have [`node.js`](https://nodejs.org/) and [`yarn`](https://yarnpkg.com/) installed on your machine.

## Installation and running the system locally

-   Install all dependencies by running `yarn`

-   Build and start the system with `yarn start` or start it in development mode, watching for changes to the source code, with `yarn start:dev`

-   Visit `http://localhost:4000` in your web browser

## Usage

At `http://localhost:4000` you will see the GraphQL Playground. You can use the left-side of the screen to create a query and on the right you can view information about the API schema and see the results of your query.

For any customer that wants to receive marketing information either every day or never, simply specify `wantsMarketing` to be `true` or `false`, respectively. For other customers you can specify the `daysOfWeek` (plural) and/or `dateOfMonth` (singular) they want to receive marketing information on. Note that `daysOfWeek` and `dateOfMonth` may both be specified on the same customer and that customer will receive marketing information on any day that meets **either** criteria, whereas specifying `wantsMarketing` for a customer with any other preference specified will result in an error being thrown.

As an example, paste in this query and hit the 'play' button:

```
{
  report(
    customerPreferences: [
      { name: "a", daysOfWeek: [MON, WED] },
      { name: "b", wantsMarketing: true },
      { name: "c", dateOfMonth: 28 },
      { name: "d", wantsMarketing: true }
    ]
  )
}
```

The output will be a JSON object with the following format:

```json
{
  "data": {
    "report": {
      "Sun 16-February-2020": [
        "b",
        "d"
      ],
      "Mon 17-February-2020": [
        "a",
        "b",
        "d"
      ],
      ...
      "Thu 14-May-2020": [
        "b",
        "d"
      ],
      "Fri 15-May-2020": [
        "b",
        "d"
      ]
    }
  }
}
```

Each key on the `report` object is a date, and each value is an array of the customer who should receive marketing information on that date.

## Aim

To build a system which will accept the choices of multiple customers about when to receive marketing information as input and outputs a report of the upcoming 90 days. For each day that marketing material will be sent, the report should show which customers will be a recipient.

## Approach

### Environment configuration

I bootstrapped a TypeScript project by following the instructions in [this article](https://khalilstemmler.com/blogs/typescript/node-starter-project/) from Khalil Stemmler's blog. I chose Khalil's guide because I've used it before so I know it works. While it doesn't include some things I knew I wanted (Jest, Apollo), adding them is pretty straight-forward and feels like the lesser of two evils when compared with taking something like [Microsoft's node starter](https://github.com/microsoft/TypeScript-Node-Starter) and removing everything I don't want.

I added [Jest](https://jestjs.io/) for testing because it's the simplest testing library I've worked with and the more frictionless test-writing is, the more tests will get written. Once again I used a config which I've used before. This one comes from [Basarat Ali Syed's enormously valuable `typescript-book`](https://github.com/basarat/typescript-book/blob/master/docs/testing/jest.md).

I added [eslint](https://eslint.org/) and [Prettier](https://prettier.io/) for linting and formatting because they're straightforward, configurable, and have such wide adoption that there's a wealth of information online about configuring them. I used [this guide from Robert Cooper](https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project) to get them setup quickly with sensible defaults. Robert's guide also includes [husky](https://github.com/typicode/husky) and [lint-staged](https://github.com/okonet/lint-staged), which will stop me commiting any code with TypeScript compilation errors, linting errors or failing tests. Well worth the extra time it takes to commit.

## Design decisions

I tried to make the same decisions I'd make "in real life" wherever practical.

For example, I decided right away to return my report in JSON format. My reason for this is that JSON is a) easy to work with and serialise into objects in lots of programming languages and b) in a "real life" situation this probably wouldn't be a standalone system—we'd want something to read the reports it generates and actually send the emails. This simplicity of that second system would depend in part of the developer-friendliness of _this_ system, and JSON is pretty developer-friendly.

I also knew I didn't want to accept either a plain string or a collection of plain strings as input to the Customer Preference Centre, for much the same reason. Code for parsing strings often feels brittle, and doing it well usually involves regular expressions, which in my experience lots of programmers dislike and therefore don't learn in much detail. I want to make my code as easy as possible to work with for the highest proportion of programmers possible.

Instead, I chose to use [GraphQL](https://graphql.org) for my API layer. Some of the usual benefits of GraphQL don't apply in this case (avoid the over- and under-fetching typical of REST APIs, for example), but being able to type inputs is definitely of benefit here. It promotes consistency between data models at every layer of an application and even between applications, which makes achieving ubiquitous language much easier.

I opted to use [type-graphql](https://typegraphql.ml) to build my GraphQL schema and [Apollo](https://www.apollographql.com/docs/apollo-server/) to run my GraphQL server. `type-graphql` lets me use TypeScript class definitons as GraphQL input and return types, rather than defining them separately in the [GraphQL Schema Definition Language](https://alligator.io/graphql/graphql-sdl/). This further encourages consistency, as any future changes only need to get made in one place. In this particular use case, using `type-graphql` wasn't a completely straightforward choice as it specifically generates schemas based on TypeScript classes, _not_ based on interfaces or types, and if I hadn't been using `type-graphql` then I probably would have only defined interfaces and/or types in this project.

I chose to use [moment](https://momentjs.com/) because it makes things like creating collections of dates and formatting those dates absolutely trivial. I know some people prefer other libraries for their smaller size, but I'm already familiar with `moment`, which is very valuable when there's a lot riding on a project! (And I'm not overly concerned about bundle size in this context.)

### Data models

I found modelling customer preferences to be the most challenging part of designing and building this system.

One thing that wasn't specified in the brief is whether or not the different types of preferences should be mutually exclusive. Obviously choosing "never" is mutually exclusive with anything else, and being able to choose "every day" along with some specific days or date seems pointless. But should a user be able to specify both some days of the week and a date of the month? In the end I decided yes. Restricting the user's choices should always be carefully thought about and justified, but I didn't feel that I had real justification here.

These choices about user preferences and using GraphQL makes things a little tricky, however. The GraphQL spec doesn't support any flexibility in input types beyond specifying fields as optional. Ideally I would have specified that each customer can have either:

-   an absolute preference (every day / never), or
-   one or both of a days of week preference and a date of month preference

Instead I've marked all preference types as optional and manually thrown an error if both an absolute preference is present on any customer which also has any other preference. While the need to do this feels like a limitation of GraphQL, I believe it still represents an improvement over a REST endpoint that accepts a JSON blob, as the API layer provides no typing or validation at all in that case. There is ongoing discussion in the GraphQL community about issues like this and hopefully the spec will one day account for use cases like this. [Union types](https://github.com/graphql/graphql-spec/blob/master/rfcs/InputUnion.md) as inputs seems like it will be a step in the right direction.

The other relevant limitation of GraphQL is that there isn't a _good_ way to return dynamic typed data. For example this program returns a JSON object whose keys are the dates of the next 90 days. To create a GraphQL type representing that is very difficult as the schema needs to know what those keys will be ahead of time. One option would be to have 90 keys with names from `date1` to `date90`, each with a value containing both the date and the customers to email. At this point, though, I'd be complicating things further than I feel I could justify. Instead, I used [graphql-json-type](https://github.com/taion/graphql-type-json) to specify that the API will return JSON. Again it's a shame to have to do this, but it feels like the lesser of two evils and in my opinion doesn't stop GraphQL being the best option for the API layer.

## Problems encountered

I was relying on `type-graphql`'s built-in validation to validate that `dayOfMonth` is between 1 and 28. I couldn't get the validation to fire for some reason, and I ran out of time to figure out why, so I had to implement a quick and dirty manual check in one of my type guards. If I'd had more time to spend on this project, this is what I'd have focussed on.

In one of my test files I import some JSON files of expected outputs for various function calls. Although it never stopped TypeScript compiling or the tests running, those import statements have been underlined in red in VS Code with a 'Cannot find module ...' error ever since I wrote them. I looked up the exact error online and all the solutions I found were to set `resolveJSONModule` to true in `tsconfig.json`, but I had already done that. If I'd had more time _and_ fixed the previous issue, then I'd have investigated this further.

## Reflections

First of all, I really enjoyed this challenge! I thought it was well-designed, and it forced me to make some interesting decisions (see: the section of this document on data modelling) while being straight-forward enough in concept for me to decide on an overall architecture and start writing code quite quickly.

In several areas I used libraries or configurations that I've used before because I know they work and that I could use them correctly and quickly. I certainly stand by that decision-making: there's potentially a job riding on this code, so the most important thing is to get it right! But if I did this challenge again I'd like to branch out further beyond my comfort zone and try things like using [Babel](https://babeljs.io/) to compile TypeScript or a different date library.

## Finally

Thank you for taking the time to test my code and read this whole README! I hope I've demonstrated what TotallyMoney is looking for and look forward to hearing back.
