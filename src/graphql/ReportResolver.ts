import { Resolver, Query, Arg } from 'type-graphql'
import { GraphQLJSONObject } from 'graphql-type-json'
import * as CustomerPreferences from '../models/CustomerPreferences'
import * as reportService from '../services/reportService'

@Resolver()
export class ReportResolver {
    @Query(() => GraphQLJSONObject)
    report(
        @Arg('customerPreferences', () => [CustomerPreferences.CustomerPreferences], {
            validate: true
        })
        customerPreferences: CustomerPreferences.CustomerPreferences[]
    ) {
        CustomerPreferences.validateCustomerPreferences(customerPreferences)
        return reportService.createReport(customerPreferences)
    }
}
