import 'reflect-metadata'

import { ApolloServer } from 'apollo-server'
import { buildSchema } from 'type-graphql'
import { ReportResolver } from './graphql/ReportResolver'

async function main() {
    const schema = await buildSchema({ resolvers: [ReportResolver] })
    const server = new ApolloServer({ schema, playground: true })

    server.listen().then(({ url }) => {
        console.log(`🚀  Server ready at ${url}`)
    })
}

main()
