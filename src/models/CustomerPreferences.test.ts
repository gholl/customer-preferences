import 'reflect-metadata'

import * as CustomerPreferences from './CustomerPreferences'

describe('type guards', () => {
    test('should return true for valid DaysCustomers, DateCustomers and AbsoluteCustomers', () => {
        const daysCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'A',
            daysOfWeek: [CustomerPreferences.DayOfWeek.MON, CustomerPreferences.DayOfWeek.TUE]
        }
        const dateAndDaysCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'B',
            dateOfMonth: 12,
            daysOfWeek: [CustomerPreferences.DayOfWeek.WED]
        }
        const absoluteCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'C',
            wantsMarketing: true
        }

        expect(CustomerPreferences.hasDaysPreference(daysCustomer)).toBe(true)
        expect(CustomerPreferences.hasDatePreference(dateAndDaysCustomer)).toBe(true)
        expect(CustomerPreferences.hasAbsolutePreference(absoluteCustomer)).toBe(true)
    })

    test('should return false for incorrect customer types', () => {
        const daysCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'A',
            daysOfWeek: [CustomerPreferences.DayOfWeek.MON, CustomerPreferences.DayOfWeek.TUE]
        }
        const dateAndDaysCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'B',
            dateOfMonth: 12,
            daysOfWeek: [CustomerPreferences.DayOfWeek.WED]
        }
        const absoluteCustomer: CustomerPreferences.CustomerPreferences = {
            name: 'C',
            wantsMarketing: true
        }

        expect(CustomerPreferences.hasDatePreference(daysCustomer)).toBe(false)
        expect(CustomerPreferences.hasAbsolutePreference(dateAndDaysCustomer)).toBe(false)
        expect(CustomerPreferences.hasDaysPreference(absoluteCustomer)).toBe(false)
    })
})

describe('validate preferences', () => {
    test('should throw an error for customers with absolute preference and any other preferences', () => {
        const shouldThrow = () =>
            CustomerPreferences.validateCustomerPreferences([
                { name: 'A', wantsMarketing: true, dateOfMonth: 1 }
            ])

        expect(shouldThrow).toThrow()
    })
    test('should not throw for valid preference combinations', () => {
        const shouldNotThrow = () =>
            CustomerPreferences.validateCustomerPreferences([
                { name: 'A', wantsMarketing: true },
                { name: 'B', daysOfWeek: [CustomerPreferences.DayOfWeek.MON] }
            ])

        expect(shouldNotThrow).not.toThrow()
    })
})
