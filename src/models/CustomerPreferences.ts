import { InputType, Field, registerEnumType, Int } from 'type-graphql'

import { Max, Min, ArrayNotEmpty } from 'class-validator'

export enum DayOfWeek {
    MON = 'MON',
    TUE = 'TUE',
    WED = 'WED',
    THU = 'THU',
    FRI = 'FRI',
    SAT = 'SAT',
    SUN = 'SUN'
}
registerEnumType(DayOfWeek, { name: 'DayOfWeek' })

interface BaseCustomer {
    name: string
}

export interface DaysCustomerPreference extends BaseCustomer {
    daysOfWeek: string[]
}

export interface DateCustomerPreference extends BaseCustomer {
    dateOfMonth: number
}

export interface AbsoluteCustomerPreference extends BaseCustomer {
    wantsMarketing: boolean
}

export const hasDaysPreference = (customer: any): customer is DaysCustomerPreference => {
    return customer.daysOfWeek !== undefined
}

export const hasDatePreference = (customer: any): customer is DateCustomerPreference => {
    const dateOfMonth: number = customer.dateOfMonth
    if (dateOfMonth !== undefined && !(dateOfMonth >= 1 && dateOfMonth <= 28))
        throw new Error(
            `Got unexpected value ${dateOfMonth} for ${customer.name}.dateOfMonth. dateOfMoth should be a number from 1 to 28`
        )
    return dateOfMonth !== undefined
}

export const hasAbsolutePreference = (customer: any): customer is AbsoluteCustomerPreference => {
    return customer.wantsMarketing !== undefined
}

export const validateCustomerPreferences = (
    customerPreferencesArr: CustomerPreferences[]
): void => {
    const invalidPreferences: CustomerPreferences[] = []

    customerPreferencesArr.forEach(cp => {
        if (hasAbsolutePreference(cp) && (hasDatePreference(cp) || hasDaysPreference(cp))) {
            invalidPreferences.push(cp)
        }
    })

    if (invalidPreferences.length) {
        throw new Error(
            `Customers ${formatNames(
                invalidPreferences
            )} have invalid preferences. Absolute preference 'wantsMarketing' is incompatible with any other preferences.`
        )
    }
}

const formatNames = (customerPreferencesArr: CustomerPreferences[]): string => {
    const arrayString = customerPreferencesArr.reduce((acc, cp, i, arr) => {
        const isLast = i === arr.length - 1
        const result = isLast ? cp.name : cp.name + ', '
        return acc + result
    }, '')
    return `[${arrayString}]`
}

@InputType('Customer', {
    description:
        "Describes a customer's name and their preferences for when to receive marketing information. Customers will receive marketing information on every day that meets any of their stated criteria. NOTE: `wantsMarketing` is incompatible with other preferences."
})
export class CustomerPreferences {
    @Field()
    name!: string

    @Field(() => [DayOfWeek], { nullable: true })
    @ArrayNotEmpty()
    daysOfWeek?: DayOfWeek[]

    @Field(() => Int, {
        nullable: true,
        description:
            'An integer between 1 and 28 inclusive, representing a date each month to receive marketing information'
    })
    @Max(28)
    @Min(1)
    dateOfMonth?: number

    @Field(() => Boolean, {
        nullable: true,
        description:
            '`true` to receive all marketing information, `false` to receive none. This is incompatible with other preferences.'
    })
    wantsMarketing?: boolean
}
