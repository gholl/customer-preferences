import Moment from 'moment'
import * as service from './reportService'
import * as CustomerPreferences from '../models/CustomerPreferences'
// TODO: Figure out why VS Code is showing me an error for JSON imports even though TypeScript is handling them
import reportDates from '../testData/reportDates.json'
import dayOfWeekResults from '../testData/daysOfWeekResult.json'
import dateOfMonthResult from '../testData/dateOfMonthResult.json'
import absoluteResult from '../testData/absoluteResult.json'

const mockMoment = () => {
    jest.spyOn(service, 'getNow').mockImplementation(() =>
        Moment(new Date('February 11, 2020, 20:05:00'))
    )
}

describe('getEmptyReport', () => {
    beforeAll(() => {
        mockMoment()
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    test('should return an array of the next 90 days ', () => {
        const result = service.getReportMoments().map(day => day.format('ddd DD-MMMM-YYYY'))

        expect(result).toEqual(reportDates)
    })
})

describe('createReport', () => {
    beforeAll(() => {
        mockMoment()
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    test('should handle daysOfWeek preference', () => {
        const report = service.createReport([
            {
                name: 'A',
                daysOfWeek: [CustomerPreferences.DayOfWeek.MON, CustomerPreferences.DayOfWeek.TUE]
            },
            { name: 'B', daysOfWeek: [CustomerPreferences.DayOfWeek.TUE] },
            {
                name: 'C',
                daysOfWeek: [
                    CustomerPreferences.DayOfWeek.WED,
                    CustomerPreferences.DayOfWeek.THU,
                    CustomerPreferences.DayOfWeek.SAT
                ]
            },
            { name: 'D', daysOfWeek: [CustomerPreferences.DayOfWeek.THU] },
            {
                name: 'E',
                daysOfWeek: [CustomerPreferences.DayOfWeek.FRI, CustomerPreferences.DayOfWeek.SUN]
            },
            { name: 'F', daysOfWeek: [CustomerPreferences.DayOfWeek.SAT] },
            { name: 'G', daysOfWeek: [CustomerPreferences.DayOfWeek.SUN] }
        ])

        expect(report).toEqual(dayOfWeekResults)
    })

    test('should handle dateOfMonth preference', () => {
        const report = service.createReport([
            { name: 'A', dateOfMonth: 1 },
            { name: 'B', dateOfMonth: 2 },
            { name: 'C', dateOfMonth: 3 },
            { name: 'D', dateOfMonth: 4 },
            { name: 'E', dateOfMonth: 5 },
            { name: 'F', dateOfMonth: 6 },
            { name: 'G', dateOfMonth: 7 }
        ])

        expect(report).toEqual(dateOfMonthResult)
    })

    test('should handle absolute preference', () => {
        const report = service.createReport([
            { name: 'A', wantsMarketing: true },
            { name: 'B', wantsMarketing: true },
            { name: 'C', wantsMarketing: false },
            { name: 'D', wantsMarketing: false },
            { name: 'E', wantsMarketing: false },
            { name: 'F', wantsMarketing: false },
            { name: 'G', wantsMarketing: true }
        ])

        expect(report).toEqual(absoluteResult)
    })
})
