import 'reflect-metadata'
import Moment from 'moment'
import * as CustomerPreferences from '../models/CustomerPreferences'

const REPORTING_PERIOD_NUM_DAYS = 90

interface Report {
    [key: string]: string[]
}

const dayMap: { [key: number]: string } = {
    0: 'SUN',
    1: 'MON',
    2: 'TUE',
    3: 'WED',
    4: 'THU',
    5: 'FRI',
    6: 'SAT'
}

export const getNow = (): Moment.Moment => Moment()

export const getReportMoments = (): Moment.Moment[] => {
    const now = getNow()
    const moments = []
    for (let i = 1; i <= REPORTING_PERIOD_NUM_DAYS; i++) {
        moments.push(Moment(now.add(1, 'day')))
    }

    return moments
}

export const createReport = (
    customerPreferencesArray: CustomerPreferences.CustomerPreferences[]
) => {
    const moments = getReportMoments()

    return moments.reduce((report, moment) => {
        report[moment.format('ddd DD-MMMM-YYYY')] = customerPreferencesArray
            .filter(customer => {
                if (CustomerPreferences.hasAbsolutePreference(customer)) {
                    return customer.wantsMarketing
                }

                let shouldSend = true

                if (CustomerPreferences.hasDaysPreference(customer)) {
                    shouldSend =
                        shouldSend &&
                        customer.daysOfWeek.some(dayOfWeek => dayOfWeek === dayMap[moment.day()])
                }
                if (CustomerPreferences.hasDatePreference(customer)) {
                    shouldSend = shouldSend && customer.dateOfMonth === moment.date()
                }

                return shouldSend
            })
            .map(customer => customer.name)
        return report
    }, {} as Report)
}
